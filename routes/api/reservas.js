var express = require('express');
var router = express.Router();
var reservaController = require('../../controllers/api/reservaControllerApi');

router.get('/', reservaController.reserva_list);
router.post('/create', reservaController.reservas_create);

module.exports = router;