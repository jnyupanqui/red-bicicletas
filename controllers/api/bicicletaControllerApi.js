var Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function (req, res) {
    res.status(200).json({
        bicicletas: Bicicleta.allBicis
    });
}

exports.create = function (req, res) {
    var bici = new Bicicleta({code: req.body.code, color: req.body.color, modelo: req.body.modelo, ubicacion: [req.body.lat, req.body.long]});
    bici.save(function(err, result) {
        res.status(200).json({
            bicicleta: result
        });
    });


}

exports.delete = function (req, res) {
    Bicicleta.removeById(req.body.id);
    res.status(204).send();
}
