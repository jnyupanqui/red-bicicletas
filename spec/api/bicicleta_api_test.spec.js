var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var server = require('../../bin/www');
var request = require('request');

var base_url = 'http://localhost:3000/api/v0/bicicletas';

describe('Bicicleta API', () => {
    beforeEach(function(done) {
        var mongoDB = 'mongodb://localhost:27017/bicicleta';
        mongoose.connect(mongoDB, { useNewUrlParser: true });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function() {
            console.log('We are connected to test database!');
            done();
        });
    });

    afterEach(function(done) {
        Bicicleta.deleteMany({}, function(err, success) {
            if(err) console.log(err);
            done();
        });
    });

    describe('GET BICICLETAS /', () => {
        it('Status 200'), (done) => {
            request.get(base_url, function(error, response, body) {
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.Bicicleta.length).toBe(0);
                done();
            });
        };
    });

    describe('POST BICICLETAS /create', () => {
        it('Status 200', (done) => {
            var headers = {'content-type': 'application/json'};
            var aBici = '{"code": 10, "color": "rojo", "modelo": "urbano", "lat": -34, "long": -54}';
            request.post({
                headers: headers,
                url: base_url + '/create',
                body: aBici
            }, function(error, response, body) {
                expect(response.statusCode).toBe(200);
                var bici = JSON.parse(body).bicicleta;

                console.log(bici);
                
                expect(bici.color).toBe('rojo');
                expect(bici.ubicacion[0]).toBe(-34);
                expect(bici.ubicacion[1]).toBe(-54);
                done();
                
            });
        })
    });
});



// var Bicicleta = require('../../models/bicicleta');
// var request = require('request')

// var server = require('../../bin/www');

// beforeEach(() => console.log('testeando…'));

// describe('Bicicleta API', () => {
//     describe('Get Bicicletas /', () => {
//         it('Status 200', async () => {
//             expect(Bicicleta.allBicis.length).toBe(0);

//             var a = new Bicicleta(1, 'negro', 'urbana', [-34.6012424, -58.3861497]);
//             Bicicleta.add(a);

//             await request.get('http://localhost:3000/api/v0/bicicletas', function (error, response) {
//                 expect(response.statusCode).toEqual(200);
//             });
//         });
//     });

//     describe('Post Bicicletas /', () => {
//         it('Status 200', async () => {
//             var headers = {'content-type': 'application/json'};
//             var aBici = '{"id": 10, "color": "rojo", "modelo": "urbana", "lat": -34, "long": -54}';

//             await request.post({
//                 headers: headers,
//                 url: 'http://localhost:3000/api/v0/bicicletas',
//                 body: aBici
//             }, function (error, response, body) {
//                 expect(response.statusCode).toBe(200);
//                 expect(Bicicleta.findById(10).color).toBe("rojo");
//             });
//         });
//     });
// });
