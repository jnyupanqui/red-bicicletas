var express = require('express');
var router = express.Router();
var bicicletaController = require('../../controllers/api/bicicletaControllerApi');

router.get('/', bicicletaController.bicicleta_list);

router.post('/create', bicicletaController.create);

router.delete('/', bicicletaController.delete);

module.exports = router;