var mongoose = require('mongoose');
var Reserva = require('./reserva');
var Schema = mongoose.Schema;

var uniqueValidator = require('mongoose-unique-validator');

var bcrypt = require('bcrypt');
const saltRounds = 10;
const Token = require('../models/token');
const mailer = require('../services/mail');
const crypto = require('crypto');

const validateEmail = function (email) {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

var usuarioSchema = new Schema({
    nombre: {
        type: String,
        trim: true,
        required: [true, 'EL nombre es obligatorio']
    },
    email: {
        type: String,
        trim: true,
        required: [true, 'EL email es obligatorio'],
        lowercase: true,
        unique: true,
        validate: [validateEmail, 'Por favor ingrese un email valido'],
        match: [/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/]
    },
    password: {
        type: String,
        required: [true, 'El password es obligatorio']
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    usuarioVerificado: {
        type: Boolean,
        default: false
    },
    googleId: String,
    facebookId: String
});


usuarioSchema.pre('save', function (next) {
    if (this.isModified('password')) {
        this.password = bcrypt.hashSync(this.password, saltRounds);
    }
    next();
});

usuarioSchema.plugin(uniqueValidator, { message: 'El {PATH} ya existe con otro usuario.' });

usuarioSchema.methods.validPassword = function (password) {
    return bcrypt.compareSync(password, this.password);
}

usuarioSchema.methods.reservar = function (biciId, desde, hasta, cb) {
    var reserva = new Reserva({ usuario: this._id, bicicleta: biciId, desde: desde, hasta: hasta });
    console.log(reserva);
    reserva.save(cb);
}

usuarioSchema.methods.enviar_email_bienvenida = function (cb) {
    const token = new Token({ _userId: this._id, token: crypto.randomBytes(16).totring('hex') });
    const email_destination = this.email;
    token.save(function (err) {
        if (err) {
            return console.log(err.message);
        }
        const mailOptions = {
            from: 'no-reply@redbicicletas.com',
            to: email_destination,
            subject: 'Verificacion de cuenta',
            text: 'Hola,\n\n' + 'Por favor, para verificar su cuenta haga click en este link: \n' + 'http://localhost:3000' + '\/token/configuration\/' + token.token + '.\n'
        };

        mailer(mailOptions, function (err) {
            if (err) { return console.log(err.message); }

            console.log('A verification email has been sent to ' + email_destination + '.');
        });
    });
}


usuarioSchema.statics.findOneOrCreateByFacebook = function findOneOrCreate(condition, callback) {
    const self = this;
    console.log(condition);
    self.findOne({
        $or: [
            { 'facebookId': condition.id }, { 'email': condition.emails[0].value }
        ]
    }).then((result) => {
        if (result) {
            callback(result)
        } else {
            console.log('----condition------');
            console.log(condition);
            let values = {};
            values.facebookId = condition.id;
            values.email = condition.emails[0].value;
            values.nombre = condition.displayName || 'SIN NOMBRE';
            values.verificado = true;
            values.password = crypto.randomBytes(16).toString('hex');
            console.log('-values-----------');
            console.log(values)
            self.create(values)
                .then((result) => {
                    return callback(err, result);
                })
                .catch((err) => {
                    console.log(err);
                })
        }
    }).catch((err) => {
        callback(err);
        console.error(err);
    })
}


usuarioSchema.statics.findOneOrCreateByGoogle = function findOrCreate(condition, callback) {
    const self = this;
    console.log(condition);
    self.findOne({
        $or: [
            { 'googleId': condition.id }, { 'email': condition.emails[0].value }
        ]
    }).then((result) => {
        if (result) {
            callback(result);
        } else {
            console.log('---------CONDITION-------------');
            console.log(condition);
            var values = {}
            values.googleId = condition.id;
            values.email = condition.emails[0].value;
            values.nombre = condition.displayName || 'SIN NOMBRE';
            values.verificado = true;
            values.password = condition.id;
            console.log('-------VALUES-------------');
            console.log(values);

            self.create(values)
                .then((result) => {
                    return callback(err, result);
                })
                .catch((err) => {
                    console.log(err);
                })
        }
    }).catch((err) => {
        callback(err);
        console.error(err);
    })
}
module.exports = mongoose.model('Usuario', usuarioSchema);