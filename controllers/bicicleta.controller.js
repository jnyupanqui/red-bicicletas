var Bicicleta = require('../models/bicicleta');

module.exports.bicicleta_list = function(req, res) {
        res.render('bicicletas/index', { bicis: Bicicleta.allBicis});
}

module.exports.bicicleta_create_get = function(req, res) {
    res.render('bicicletas/create');
}


module.exports.bicicleta_create_post = function(req, res) {
    var bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.lat, req.body.long];
    Bicicleta.add(bici);
    res.redirect('/bicicletas');
}

module.exports.bicicleta_delete_post = function(req, res) {
    Bicicleta.removeById(req.params.id);

    res.redirect('/bicicletas');
}

module.exports.bicicleta_update_get = function(req, res) {
    var bici = Bicicleta.findById(req.params.id);
    res.render('bicicletas/update', { bici: bici });
}

module.exports.bicicleta_update_post = function(req, res) {
    var bici = Bicicleta.findById(req.params.id);
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat, req.body.long];
     
    res.redirect('/bicicletas');
}