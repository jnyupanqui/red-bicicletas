require('dotenv').config();

require('newrelic');
const passport = require('passport');
var GoogleStrategy = require('passport-google-oauth20').Strategy;
const session = require('express-session');
const MongoDBStore = require('connect-mongodb-session')(session);

var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var biciRouter = require('./routes/bicicletas');

var bicicletasAPIRouter = require('./routes/api/bicicletas');
var usuariosAPIRouter = require('./routes/api/usuarios');
var reservasAPIRouter = require('./routes/api/reservas');

let store;
if (process.env.NODE_ENV === 'development') {
  store = new session.MemoryStore;
} else {
  store = new MongoDBStore({
    uri: process.env.MONGO_URI,
    collection: 'sessions'
  });
  store.on('error', function (error) {
    assert.ifError(error);
    assert.ok(false);
  });
}

passport.use(new GoogleStrategy({
  clientID: process.env.GOOGLE_CLIENT_ID,
  clientSecret: process.env.GOOGLE_CLIENT_SECRET,
  callbackURL: "http://www.example.com/auth/google/callback"
},
  function (accessToken, refreshToken, profile, cb) {
    User.findOrCreate({ googleId: profile.id }, function (err, user) {
      return cb(err, user);
    });
  }
));

var app = express();

const mongoose = require('mongoose');
const { assert } = require('console');
mongoose.connect(process.env.MONGO_URI, { useNewUrlParser: true, useUnifiedTopology: true });
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error: '))

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/bicicletas', biciRouter);
app.use('/api/v0/bicicletas', bicicletasAPIRouter);
app.use('/api/v0/usuarios', usuariosAPIRouter);
app.use('/api/v0/reservas', reservasAPIRouter);
app.use('/privacy_policy', (req, res) => {
  console.log(__dirname)
  res.sendFile(__dirname + '/public/policy_privacy.html');
});
app.use('/google796a748676129cc4.html', (req, res) => {
  console.log(__dirname)
  res.sendFile(__dirname + '/public/google796a748676129cc4.html');
});

app.get('/auth/google',
  passport.authenticate('google', { scope: [
    'https://www.googleapis.com/auth/plus.login',
    'https://www.googleapis.com/auth/plus.profile.emails.read'] }));

app.get('/auth/google/callback', passport.authenticate('google', { 
    successRedirect: '/',
    failureRedirect: '/error'
   })
  );

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
